public class Arvuttaja {
  private class Memento{
    private int number;
    public Memento(int n){
      this.number = n;
      System.out.println(n);
    }
    public int getNumber() {
         return number;
     }
  }
  public Object liityPeliin () {
    int max = 10;
    int min = 1;
    int range = max - min + 1;

    int rand = (int)(Math.random() * range) + min;
    Memento memento = new Memento(rand);
    return memento;
  }
  public void arvaus(Object  obj, int a ){
    Memento memento = (Memento) obj;
    if (memento.getNumber() == a) {
      System.out.println("correct");
    }
    else {
      System.out.println("wrong");

    }
  }
}
